# Quick Start #

This quick guide will help you run the app in your local environment.

### Requirements ###

* Docker
* Docker Compose
* yarn
* pm2

### Set Up ###

#### Run docker containers (http, php, mariadb, redis) ####

```
docker-compose up -d
```

#### Run in docker container (uspotted-php) ####

```
composer install --optimize-autoloader --no-dev

cp .env.example .env

php artisan key:generate
php artisan migrate:install
php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"
php artisan migrate
```

#### Run locally ####

```
cd apps/react-front-app

node_modules/laravel-echo-server/bin/server.js init

pm2 start yarn --name app -- start
pm2 start node_modules/laravel-echo-server/bin/server.js --name echo-server -- start
```

### Optimization (uspotted-php container) ###

```
php artisan config:cache
php artisan route:cache
```

### Running Applications ###

* http://localhost:3000 (Front App)
* http://localhost:8080 (Rest API)