<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Repositories\AuthRepository;
use App\Repositories\TokenRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use App\Events\UserLoggedIn;

class AuthController extends Controller
{
    public function __construct(
        public AuthRepository  $authRepository,
        public TokenRepository $tokenRepository
    ) {}

    public function login(LoginRequest $request): JsonResponse
    {
        $token = $this->authRepository->loginAndGetToken($request->validated());

        UserLoggedIn::dispatch(Auth::user());

        return Response::json([
            'token_type'   => 'Bearer',
            'access_token' => $token,
            'message'      => trans('auth.messages.login')
        ], JsonResponse::HTTP_OK);
    }

    public function logout(Request $request): JsonResponse
    {
        $this->tokenRepository->deleteAllForUser($request->user());

        return Response::json([
            'message' => trans('auth.messages.logout')
        ], JsonResponse::HTTP_OK);
    }

    public function user(Request $request): UserResource
    {
        return new UserResource($request->user());
    }
}
