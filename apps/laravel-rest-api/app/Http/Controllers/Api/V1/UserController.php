<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    public function __construct(
        public UserRepository  $userRepository
    ) {}

    public function index(): ResourceCollection
    {
        return UserResource::collection($this->userRepository->all());
    }

    public function show(int $id): UserResource|HttpResponseException
    {
        try {
            return new UserResource($this->userRepository->find($id));
        } catch (\Exception $e) {
            throw new HttpResponseException(
                Response::json([
                    'message' => trans('user.messages.not_found')
                ], JsonResponse::HTTP_NOT_FOUND)
            );
        }
    }
}
