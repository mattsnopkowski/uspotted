<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRelationRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserRelatedResource;
use App\Repositories\UserRelatedRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Exceptions\UserRelatedRelationExists;
use App\Exceptions\UserRelatedRelationNotFoundException;

class UserRelatedController extends Controller
{
    public function __construct(
        public UserRelatedRepository $userRelatedRepository
    ) {}

    public function index(Request $request, string $relation = null): ResourceCollection
    {
        return UserRelatedResource::collection(
            $this->userRelatedRepository->all($request->user(), $relation)
        );
    }

    public function show(Request $request, int $id): UserRelatedResource|HttpResponseException
    {
        try {
            return new UserRelatedResource($this->userRelatedRepository->find($request->user(), $id));
        } catch (\Exception $e) {
            throw new HttpResponseException(
                Response::json([
                    'message' => trans('user.related.messages.not_found')
                ], JsonResponse::HTTP_NOT_FOUND)
            );
        }
    }

    public function store(StoreUserRelationRequest $request): JsonResponse
    {
        try {
            $this->userRelatedRepository->storeRelation($request->user(), $request->validated());
            return Response::json([
                'message' => trans('user.related.messages.relation_created')
            ], JsonResponse::HTTP_OK);
        } catch (UserRelatedRelationExists $e) {
            return Response::json([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_NOT_FOUND);
        } catch (\Exception $e) {
            throw new HttpResponseException(
                Response::json([
                    'message' => trans('user.related.messages.error_on_creation')
                ], JsonResponse::HTTP_BAD_REQUEST)
            );
        }
    }

    public function destroy(Request $request, int $id): JsonResponse
    {
        try {
            $this->userRelatedRepository->deleteRelation($request->user(), $id);
            return Response::json([
                'message' => trans('user.related.messages.relation_deleted')
            ], JsonResponse::HTTP_OK);
        } catch (UserRelatedRelationNotFoundException $e) {
            return Response::json([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_NOT_FOUND);
        } catch (\Exception) {
            throw new HttpResponseException(
                Response::json([
                    'message' => trans('user.related.messages.error_on_deletion')
                ], JsonResponse::HTTP_BAD_REQUEST)
            );
        }
    }
}
