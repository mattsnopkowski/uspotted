<?php

namespace App\Repositories;

use App\Repositories\UserRepository;
use App\Repositories\TokenRepository;
use Illuminate\Support\Facades\Auth;

class AuthRepository
{
    public function __construct(
        public UserRepository $userRepository,
        public TokenRepository $tokenRepository
    ) {}

    public function loginAndGetToken(array $data): string
    {
        $user = $this->userRepository->firstOrCreate($data);

        Auth::login($user);

        $this->tokenRepository->deleteAllForUser($user);

        return $this->tokenRepository->createForUser($user);
    }
}
