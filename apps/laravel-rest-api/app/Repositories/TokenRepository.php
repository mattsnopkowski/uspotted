<?php

namespace App\Repositories;

use App\Models\User;

class TokenRepository
{
    public function createForUser(User $user): string
    {
        return $user->createToken('auth_token')->plainTextToken;
    }

    public function deleteAllForUser(User $user): void
    {
        $user->tokens()->delete();
    }
}
