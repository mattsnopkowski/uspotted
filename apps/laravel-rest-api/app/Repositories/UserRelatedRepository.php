<?php

namespace App\Repositories;

use App\Exceptions\UserRelatedRelationExists;
use App\Exceptions\UserRelatedRelationNotFoundException;
use App\Models\User;
use App\Models\UserRelated;
use Illuminate\Database\Eloquent\Collection;
use App\Exceptions\UserRelatedSelfRelationException;

class UserRelatedRepository
{
    public function all(User $user, string|null $relation = null): Collection
    {
        $relations = $user->relations();

        if (in_array($relation, UserRelated::RELATIONS)) {
            $relations = $relations->where('relation', $relation);
        }

        return $relations->get();
    }

    public function find(User $user, int $id): UserRelated
    {
        return $user->relations()->findOrFail($id);
    }

    public function storeRelation(User $user, array $data): void
    {
        if ($user->id === (int) $data['related_id']) {
            throw new UserRelatedSelfRelationException(
                trans('user.related.exceptions.self_relation')
            );
        }

        $relation = $user->relations()
                         ->where('related_id', $data['related_id'])
                         ->first();

        if ($relation) {
            // $relation->update(['relation' => $data['relation']]);
            throw new UserRelatedRelationExists(
                trans('user.related.exceptions.relation_exists')
            );
        } else {
            $user->relations()->create($data);
        }
    }

    public function deleteRelation(User $user, int $id): void
    {
       $relation = $user->relations()
                        ->where('id', $id)
                        ->first();

       if ( ! $relation) {
           throw new UserRelatedRelationNotFoundException(
               trans('user.related.exceptions.not_found')
           );
       }

       $relation->delete();
    }
}
