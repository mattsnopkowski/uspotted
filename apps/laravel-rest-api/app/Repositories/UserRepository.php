<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository
{
    public function firstOrCreate(array $data): User
    {
        return User::firstOrCreate([
            'name' => $data['name']
        ]);
    }

    public function find(int $id): User|null
    {
        return User::findOrFail($id);
    }

    public function all(): Collection|null
    {
        return User::all();
    }

}
