<?php

return [
    'messages'  => [
        'not_found'   => 'User not found'
    ],

    'related' => [
        'messages'    => [
            'not_found'         => 'User relation not found',
            'relation_created'  => 'User relation has been updated',
            'error_on_creation' => 'Error occurred while adding user relation',
            'relation_deleted'  => 'User relation has been deleted',
            'error_on_deletion' => 'Error occurred while deleting user relation'
        ],
        'exceptions'  => [
            'self_relation'     => 'You cannot add self relation',
            'not_found'         => 'User relation not found',
            'relation_exists'   => 'User relation already exists'
        ]
    ]
];
