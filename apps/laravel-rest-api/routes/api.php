<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Api\V1\UserRelatedController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function() {

    Route::prefix('auth')->group(function() {
        Route::post('/login', [AuthController::class, 'login']);
        Route::middleware('auth:sanctum')->group(function() {
            Route::post('/logout', [AuthController::class, 'logout']);
            Route::get('/user', [AuthController::class, 'user']);
        });

    });

    Route::middleware('auth:sanctum')->group(function() {
        Route::prefix('/users')->group(function () {
            Route::get('/{user}', [UserController::class, 'show']);
            Route::get('/', [UserController::class, 'index']);
        });

        Route::prefix('/user/related')->group(function () {
            Route::get('/{id}', [UserRelatedController::class, 'show'])
                ->where(['id' => '[0-9]+']);
            Route::get('/{relation?}', [UserRelatedController::class, 'index']);
            Route::post('/', [UserRelatedController::class, 'store']);
            Route::delete('/{id}', [UserRelatedController::class, 'destroy']);
        });
    });
});

