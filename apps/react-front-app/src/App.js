// routes
import Router from './routes';
// theme
import ThemeConfig from './theme';
import GlobalStyles from './theme/globalStyles';
// components
import ScrollToTop from './components/ScrollToTop';
import { BaseOptionChartStyle } from './components/charts/BaseOptionChart';
import { UserContext } from './components/app/user';

// ----------------------------------------------------------------------

export default function App() {
  const user = {
    name: 'Matt'
  };

  return (
    <UserContext.Provider value={user}>
      <ThemeConfig>
        <ScrollToTop />
        <GlobalStyles />
        <BaseOptionChartStyle />
        <Router />
      </ThemeConfig>
    </UserContext.Provider>
  );
}
