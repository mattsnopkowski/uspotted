// ----------------------------------------------------------------------

const account = {
  id: 1,
  username: 'Mateusz',
  avatar: '/static/mock-images/avatars/avatar_default.jpg',
  token: '9e0ef834c3df39f25ec87dfcfd514056'
};

export default account;
