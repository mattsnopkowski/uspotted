import { useEffect, useState } from 'react';
import { Card, Typography, CardHeader, CardContent, Avatar, Box, Stack } from '@mui/material';
import { styled } from '@mui/material/styles';
import { sentenceCase } from 'change-case';
import Label from '../../Label';
import account from '../../../_mocks_/account';

const AccountStyle = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  borderRadius: theme.shape.borderRadius
}));

export default function SelectedUser(props) {
  const [relation, setRelation] = useState('');

  // useEffect(() => {
  //   fetch(`/api/relation/${props.id}`)
  //     .then((res) => res.json())
  //     .then((json) => {
  //       setRelation(json?.relation?.relationship_type ?? '');
  //     });
  // }, []);

  return (
    <Card
      sx={{
        '& .MuiTimelineItem-missingOppositeContent:before': {
          display: 'none'
        },
        mb: 3
      }}
    >
      <CardHeader title="Selected User" />
      <CardContent>
        <AccountStyle>
          <Avatar src={account.avatar} alt="photoURL" />
          <Box sx={{ ml: 2 }}>
            <Stack direction="row" spacing={2}>
              <Typography variant="subtitle2" sx={{ color: 'text.primary' }}>
                {account.username}
              </Typography>
              {relation && (
                <Label variant="ghost" color={(relation === 'enemy' && 'error') || 'success'}>
                  {sentenceCase(relation)}
                </Label>
              )}
            </Stack>
          </Box>
        </AccountStyle>
      </CardContent>
    </Card>
  );
}
