import React, { useState, useEffect } from 'react';
import { Map, Marker } from 'pigeon-maps';
import { Card, CardHeader, Box } from '@mui/material';
import Echo from 'laravel-echo';
import Socketio from 'socket.io-client';
import axios from 'axios';

export default function UsersOnlineMap() {
  const [users, setUsers] = useState([]);
  const token = '';

  useEffect(() => {
    // axios.get('/users-online').then(response => {
    //   this.setUsers({});
    // });
    const echo = new Echo({
      broadcaster: 'socket.io',
      client: Socketio,
      host: `${window.location.hostname}:6001`,
      auth: {
        headers: {
          // Authorization: 'Bearer ' + token
        }
      }
    });
    echo.private('users-online').listen('user.connected', (e) => {
      console.log(e);
      // this.setState({
      //   })
      // });
    });
  });

  const defaultCoords = { latitude: 50.02817588703076, longitude: 22.03428437795911 };
  const user = {
    username: 'Mateusz',
    token: 'xyz',
    coordinates: defaultCoords
  };
  const selectedMarkerData = null;
  const [markers, setMarkers] = useState([user]);
  const [lat, setLat] = useState(defaultCoords.latitude);
  const [lng, setLng] = useState(defaultCoords.longitude);
  const [status, setStatus] = useState(null);

  const getLocation = () => {
    if (!navigator.geolocation) {
      setStatus('Geolocation is not supported by your browser');
    } else {
      setStatus('Locating...');
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setStatus(null);
          setLat(position.coords.latitude);
          setLng(position.coords.longitude);
        },
        () => {
          setStatus('Unable to retrieve your location');
        }
      );
    }
  };

  const addMarker = ({ event, latLng, pixel }) => {
    const newUser = {
      username: Math.random().toString(36).substring({ end: 8 }),
      token: Math.random().toString(36).substring({ end: 32 }),
      coordinates: { latitude: latLng[0], longitude: latLng[1] }
    };
    setMarkers([...markers, newUser]);
    // markers.push(latLng);
  };

  const blockUser = ({ anchor }) => {
    console.log(anchor);
  };

  const setMarkerData = (user) => {};

  const showData = ({ anchor }) => {};

  const hideData = ({ anchor }) => {};

  return (
    <Card
      sx={{
        '& .MuiTimelineItem-missingOppositeContent:before': {
          display: 'none'
        }
      }}
    >
      <CardHeader title="Online Map" />
      <Box sx={{ px: 3, py: 1 }}>
        <Map onClick={addMarker} height={380} defaultCenter={[lat, lng]} defaultZoom={11}>
          {markers.map(({ coordinates: { latitude, longitude } }, index) => (
            <Marker
              onClick={showData}
              onMouseOver={showData}
              onMouseOut={hideData}
              key={index}
              width={30}
              anchor={[latitude, longitude]}
            />
          ))}
        </Map>
        <div className="App">
          <button onClick={getLocation}>Get Location</button>
          <h1>Coordinates</h1>
          <p>{status}</p>
          {lat && <p>Latitude: {lat}</p>}
          {lng && <p>Longitude: {lng}</p>}
        </div>
      </Box>
    </Card>
  );
}
