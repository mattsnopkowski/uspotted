export { default as OnlineWidget } from './OnlineWidget';
export { default as FriendsWidget } from './FriendsWidget';
export { default as EnemiesWidget } from './EnemiesWidget';
