import { useState } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

import { AuthService } from '../../../services';

export default function LoginForm() {
  const MySwal = withReactContent(Swal);
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [accessToken, setAccessToken] = useState(null);

  const LoginSchema = Yup.object().shape({
    name: Yup.string().required().max(15)
  });

  const formik = useFormik({
    initialValues: {
      name
    },
    validationSchema: LoginSchema,
    onSubmit: (values, actions) => {
      AuthService.login(values)
        .then((r) => {
          console.log(r);
          navigate('/dashboard', { replace: true });
        })
        .catch((err, res) => {
          actions.setSubmitting(false);
          console.log(err, res);
          MySwal.fire({
            title: 'Success',
            text: err,
            icon: 'success'
          });
        });
    }
  });

  const { errors, touched, isSubmitting, handleSubmit, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={3} sx={{ my: 2 }}>
          <TextField
            fullWidth
            autoComplete="name"
            label="Your name"
            {...getFieldProps('name')}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />
        </Stack>

        <LoadingButton
          fullWidth
          size="large"
          type="submit"
          variant="contained"
          loading={isSubmitting}
        >
          Login
        </LoadingButton>
      </Form>
    </FormikProvider>
  );
}

// LoginForm.propTypes = {
//   setToken: PropTypes.func.isRequired
// };
