// material
import { Box, Grid, Container, Typography } from '@mui/material';
// components
import Page from '../components/Page';
import { OnlineWidget, FriendsWidget, EnemiesWidget } from '../components/app/dashboard/widgets';
import { UsersOnlineMap, SelectedUser, ConnectionLog } from '../components/app/dashboard';
// ----------------------------------------------------------------------

export default function Dashboard() {
  return (
    <Page title="Dashboard | Minimal-UI">
      <Container maxWidth="xl">
        <Box sx={{ pb: 5 }}>
          <Typography variant="h4">Hi, Welcome back</Typography>
        </Box>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={4} md={4}>
            <OnlineWidget />
          </Grid>
          <Grid item xs={12} sm={4} md={4}>
            <FriendsWidget />
          </Grid>
          <Grid item xs={12} sm={4} md={4}>
            <EnemiesWidget />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <UsersOnlineMap />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <SelectedUser id={1} />
            <SelectedUser id={2} />
            <SelectedUser id={3} />
            <ConnectionLog />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}
