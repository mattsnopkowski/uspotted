// src/server.js
import { createServer, Model, hasMany, belongsTo } from 'miragejs';
import account from './_mocks_/account';

export function makeServer({ environment = 'test' } = {}) {
  const server = createServer({
    environment,

    models: {
      user: Model.extend({
        relations: hasMany()
      }),
      relation: Model.extend({
        user: belongsTo()
      })
    },

    seeds(server) {
      // users
      server.create('user', {
        id: 1,
        username: 'Mateusz',
        avatar: '/static/mock-images/avatars/avatar_default.jpg',
        token: '9e0ef834c3df39f25ec87dfcfd514056'
      });

      server.create('user', {
        id: 2,
        username: 'Tom',
        avatar: null,
        token: 'b49be96b9ec32191a3f5f2a5bbd699e0'
      });

      server.create('user', {
        id: 3,
        username: 'Mark',
        avatar: null,
        token: '76d4336079ee9598450dff5be25e9b9b'
      });

      // relations
      server.create('relation', {
        id: 1,
        user_id: 1,
        relationship_id: 1,
        relationship_type: 'friend'
      });

      server.create('relation', {
        id: 2,
        user_id: 1,
        relationship_id: 3,
        relationship_type: 'enemy'
      });
    },

    routes() {
      this.namespace = 'api';

      this.get('/users', (schema) => schema.users.all());

      this.get('/friends', (schema) =>
        schema.relations.where({ user_id: account.id, relationship_type: 'friend' })
      );

      this.get('/enemies', (schema) =>
        schema.relations.where({ user_id: account.id, relationship_type: 'enemy' })
      );

      this.get('/relation/:id', (schema, request) =>
        schema.relations.findBy({
          user_id: account.id,
          relationship_id: parseInt(request.params.id, 10)
        })
      );
    }
  });

  return server;
}
