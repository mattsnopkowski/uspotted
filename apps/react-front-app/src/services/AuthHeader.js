export default function AuthHeader() {
  const token = JSON.parse(localStorage.getItem('access_token'));
  return token ? { Authorization: `Bearer ${token}` } : {};
}
