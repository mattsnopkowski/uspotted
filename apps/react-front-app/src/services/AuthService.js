import axios from 'axios';

class AuthService {
  API_URL = 'http://localhost:8080/api/v1';

  login = (data) =>
    axios.post(`${this.API_URL}/login`, { ...data }).then((response) => {
      if (response.data.access_token) {
        localStorage.setItem('access_token', JSON.stringify(response.data.access_token));
      }
      return response.data;
    });

  logout = () => {
    localStorage.removeItem('access_token');
  };

  getUser = () => JSON.parse(localStorage.getItem('user'));
}

export default new AuthService();
